# Luwjistik client API docs

## Get started

```shell
$ cd apidocs
$ pip install mkdocs==1.2.2
$ mkdocs serve
```

To make changes or updates are all under folder `docs`, please refer to the [original documentation here](https://www.mkdocs.org/getting-started/#theming-our-documentation)
