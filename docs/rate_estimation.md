# Rate Estimation API

Get an estimated price for a given order

## Endpoint

Currently, the rate estimation API is only available on our test server.

```
POST http://staging.brain.luwjistik.io/api/rates
```


## Headers

```
api-key: <api-key>
Content-Type: application/json
```

## API body description

| field_name                   | field_detail                             | example                                                                                                        | required                                      |
|------------------------------|------------------------------------------|----------------------------------------------------------------------------------------------------------------|-----------------------------------------------|
| consignee_name               | Consignee's full name                    | Jack Stern                                                                                                     | yes                                           |
| consignee_number             | Consignee's phone number with area code  | +628431432423                                                                                                  | yes                                           |
| consignee_address            | Consignee's full address                 | Jalan Mangga Besar III No. 17, RT 06 RW 07, Kelurahan Bedali, Kecamatan Lawang, Kab. Malang, Jawa Timur, 60256 | yes                                           |
| consignee_postal             | Consignee's postal code                  | 60256                                                                                                          | yes                                           |
| consignee_country            | Consignee's country                      | Indonesia                                                                                                      | yes                                           |
| consignee_city               | Consignee's City                         | Malang                                                                                                         | yes                                           |
| consignee_state              | Consignee's State                        | Jawa Timur                                                                                                     | no                                            |
| consignee_province           | Consignnee's Province                    | Jawa Timur                                                                                                     | yes                                           |
| consignee_email              | Consignee's email                        | theo@gmail.com                                                                                                 | yes                                           |
| length                       | Parcel's length in cm                    | 25                                                                                                             | yes                                           |
| width                        | Parcel's width in cm                     | 30                                                                                                             | yes                                           |
| height                       | Parcel's height in cm                    | 45                                                                                                             | yes                                           |
| weight                       | Parcel's weight in kg                    | 15                                                                                                             | yes                                           |
| payment_type                 | Payment type either `cod` or `prepaid`   | prepaid                                                                                                        | yes                                           |
| pickup_contact_name          | First Mile pickup contact name           | John                                                                                                           | yes, if first_mile_partner_id is filled       |
| pickup_contact_number        | First Mile pickup contact phone number   | +628431432424                                                                                                  | yes, if first_mile_partner_id is filled       |
| pickup_state                 | First Mile pickup state                  | Singapore                                                                                                      | no                                            |
| pickup_city                  | First Mile pickup city                   | Singapore                                                                                                      | yes, if first_mile_partner_id is filled       |
| pickup_province              | First Mile pickup province               | Singapore                                                                                                      | yes, if first_mile_partner_id is filled       |
| pickup_postal                | First Mile pickup postal code            | 018956                                                                                                         | yes, if first_mile_partner_id is filled       |
| pickup_country               | First Mile pickup country                | Sinagpore                                                                                                      | yes, if first_mile_partner_id is filled       |
| pickup_address               | First Mile pickup address                | 10 Bayfront Avenue Singapore 018956                                                                            | yes, if first_mile_partner_id is filled       |
| items                        | List of items within parcel              | **Please Refer to the table below for the value**                                                              | yes                                           |

### items object description

| item_field   | item_description                                                                                                                 | item_example        | required |
|--------------|----------------------------------------------------------------------------------------------------------------------------------|---------------------|----------|
| description  | Item's description                                                                                                               | iPad Pro  10.1 inch | yes      |
| quantity     | Item's quantity                                                                                                                  | 10                  | yes      |
| sku          | Item's sku                                                                                                                       | PO-31231423         | no       |
| product_code | Item's product code                                                                                                              | 446                 | no       |
| price        | Item's price per single quantity                                                                                                 | 50000               | yes      |
| currency     | Item's currency                                                                                                                  | USD                 | yes      |
| category     | Item's category (Health and Beauty, Fashion and Apparel, Electronics, Home and Living, Live style, Accessories, Food, Others)    | Electronics         | yes      |


## Request Body

```json
{
  "consignee_name": "Theo",
  "consignee_number": "+6567796883",
  "consignee_address": "37 Pasir Panjang #01-239",
  "consignee_postal": "556",
  "consignee_country": "SG",
  "consignee_city": "Singapore",
  "consignee_state": "",
  "consignee_province": "Singapore",
  "consignee_email": "theo.dev@mailinator.com",
  "length": 25,
  "width": 30,
  "height": 45,
  "weight": 15,
  "payment_type": "cod",
  "pickup_contact_name": "Doan",
  "pickup_contact_number": "+8455566955",
  "pickup_state": "",
  "pickup_city": "Da Nang",
  "pickup_province": "Da Nang",
  "pickup_postal": "700000",
  "pickup_country": "VN",
  "pickup_address": "101 Bui Quang La",
  "order_items": [
    {
      "description": "Fuhlen bluetooth mouse",
      "quantity": 11,
      "product_code": "446",
      "sku": "PO-918242213653",
      "category": "Electronics",
      "price": 55,
      "currency": "USD"
    },
    {
      "description": "Logitech MX2 Anywhere Bluetooth mouse",
      "quantity": 1200,
      "product_code": "446",
      "sku": "PO-918242213418",
      "category": "Electronics",
      "price": 600,
      "currency": "USD"
    }
  ]
}
```

## Request Response

```json
{
  "shipping_cost": "3306000",
  "total_duties": "630.3",
  "total_taxes": "191.191"
}
```
