# Luwjistik API Docs

## Base URL

This documentation will refer to `{{ base_url }}`, which can be:

- `https://beta.luwjistik.io` for testing purposes
- `https://luwjistik.io` for production purposes (**Do not send test data here**)

## API Token

Please contact your account representative for an API key.

1. In the browser navigate and login to `{{ base_url }}`

2. In the browser naviagte to `{{ base_url }}/api_key`

3. Store the `api-key`


## Authentication

Send the API key as part of the request in the `api-key` header.
