# Create Order API

Create bulk order to network partner

## Endpoint

```
POST {{base_url}}/api/v1/orders
```


## Headers

```
api-key: <api-key>
Content-Type: application/json
```

## API body description

| field_name                   | field_detail                             | example                                                                                                        | required                                      |
| ------------------------------ | ------------------------------------------                                                           | ---------------------------------------------------------------------------------------------------------------- | ----------------------------------------------- |
| order_code                     | Client's order tracking number. Plese include your given account prefix to ensure no conflicting number. | Luw27062024                                                                                                      | yes                                             |
| consignee_name                 | Consignee's full name                                                                                | Jack Stern                                                                                                       | yes                                             |
| consignee_number               | Consignee's phone number with area code                                                              | +628431432423                                                                                                    | yes                                             |
| consignee_address              | Consignee's full address                                                                             | Jalan Mangga Besar III No. 17, RT 06 RW 07, Kelurahan Bedali, Kecamatan Lawang, Kab. Malang, Jawa Timur, 60256   | yes                                             |
| consignee_postal               | Consignee's postal code                                                                              | 60256                                                                                                            | yes                                             |
| consignee_country              | Consignee's country                                                                                  | Indonesia                                                                                                        | yes                                             |
| consignee_city                 | Consignee's City                                                                                     | Malang                                                                                                           | yes                                             |
| consignee_state                | Consignee's State                                                                                    | Jawa Timur                                                                                                       | no                                              |
| consignee_province             | Consignnee's Province                                                                                | Jawa Timur                                                                                                       | yes                                             |
| consignee_email                | Consignee's email                                                                                    | theo@gmail.com                                                                                                   | yes                                             |
| length                         | Parcel's length in cm                                                                                | 25                                                                                                               | yes                                             |
| width                          | Parcel's width in cm                                                                                 | 30                                                                                                               | yes                                             |
| height                         | Parcel's height in cm                                                                                | 45                                                                                                               | yes                                             |
| weight                         | Parcel's weight in kg                                                                                | 15                                                                                                               | yes                                             |
| payment_type                   | Payment type either `cod` or `prepaid`                                                               | prepaid                                                                                                          | yes                                             |
| pickup_contact_name            | First Mile pickup contact name                                                                       | John                                                                                                             | yes, if first_mile_partner_id is filled         |
| pickup_contact_number          | First Mile pickup contact phone number                                                               | +628431432424                                                                                                    | yes, if first_mile_partner_id is filled         |
| pickup_state                   | First Mile pickup state                                                                              | Singapore                                                                                                        | no                                              |
| pickup_city                    | First Mile pickup city                                                                               | Singapore                                                                                                        | yes, if first_mile_partner_id is filled         |
| pickup_province                | First Mile pickup province                                                                           | Singapore                                                                                                        | yes, if first_mile_partner_id is filled         |
| pickup_postal                  | First Mile pickup postal code                                                                        | 018956                                                                                                           | yes, if first_mile_partner_id is filled         |
| pickup_country                 | First Mile pickup country                                                                            | Sinagpore                                                                                                        | yes, if first_mile_partner_id is filled         |
| pickup_address                 | First Mile pickup address                                                                            | 10 Bayfront Avenue Singapore 018956                                                                              | yes, if first_mile_partner_id is filled         |
| first_mile                     | send "yes" to use the first mile service                                                             | yes                                                                                                              | no                                              |
| freight_forwarder              | send "yes" to use the freight forwarding service                                                     | yes                                                                                                              | no                                              |
| custom_brokerages              | send "yes" to use the custom brokerage service                                                       | no                                                                                                               | no                                              |
| last_mile                      | send "yes" to use the last mile service                                                              | no                                                                                                               | no                                              |
| order_items                    | List of items within parcel                                                                          | **Please Refer to the table below for the value**                                                                | yes                                             |

### items object description

| item_field   | item_description                                                                                                                 | item_example        | required |
|--------------|----------------------------------------------------------------------------------------------------------------------------------|---------------------|----------|
| description  | Item's description                                                                                                               | iPad Pro  10.1 inch | yes      |
| quantity     | Item's quantity                                                                                                                  | 10                  | yes      |
| sku          | Item's sku                                                                                                                       | PO-31231423         | no       |
| product_code | Item's product code                                                                                                              | 446                 | no       |
| price        | Item's price per single quantity                                                                                                 | 50000               | yes      |
| currency     | Item's currency                                                                                                                  | USD                 | yes      |
| category     | Item's category (Health and Beauty, Fashion and Apparel, Electronics, Home and Living, Live style, Accessories, Food, Others)    | Electronics         | yes      |


## Request Body

```json
[
  {
    "order_code": "Luw202100921003",
    "consignee_name": "Theo",
    "consignee_number": "+6567796883",
    "consignee_address": "37 Pasir Panjang #01-239",
    "consignee_postal": "556",
    "consignee_country": "Singapore",
    "consignee_city": "Singapore",
    "consignee_state": "",
    "consignee_province": "Singapore",
    "consignee_email": "theo.dev@mailinator.com",
    "length": 25,
    "width": 30,
    "height": 45,
    "weight": 15,
    "payment_type": "cod",
    "pickup_contact_name": "Doan",
    "pickup_contact_number": "+8455566955",
    "pickup_state": "",
    "pickup_city": "Da Nang",
    "pickup_province": "Da Nang",
    "pickup_postal": "700000",
    "pickup_country": "Viet Nam",
    "pickup_address": "101 Bui Quang La",
    "mawb_no": "ABC982390395457",
    "hawb_no": "DEF982390395457",
    "mawb_origin": "CBA982390395457",
    "mawb_dest": "DEF982390395457",
    "flight": "AE-2093",
    "flight_date": "2021-09-29",
    "first_mile": "no",
    "freight_forwarder": "no",
    "custom_brokerages": "yes",
    "last_mile": "yes",
    "order_items": [
      {
        "description": "Fuhlen bluetooth mouse",
        "quantity": 11,
        "product_code": "446",
        "sku": "PO-918242213653",
        "category": "Electronics",
        "price": 55,
        "currency": "usd"
      },
      {
        "description": "Logitech MX2 Anywhere Bluetooth mouse",
        "quantity": 1200,
        "product_code": "446",
        "sku": "PO-918242213418",
        "category": "Electronics",
        "price": 600,
        "currency": "usd"
      }
    ]
  }
]
```

## Request Response

```json
{
  "data": [
    {
      "order_code": "Luw202100921003",
      "consignee_name": "Theo",
      "consignee_number": "+6567796883",
      "consignee_address": "37 Pasir Panjang #01-239",
      "consignee_postal": "556",
      "consignee_country": "Singapore",
      "consignee_city": "Singapore",
      "consignee_state": "",
      "consignee_province": "Singapore",
      "consignee_email": "theo.dev@mailinator.com",
      "length": "25.0",
      "width": "30.0",
      "height": "45.0",
      "weight": "15.0",
      "payment_type": "cod",
      "pickup_contact_name": "Doan",
      "pickup_contact_number": "+8455566955",
      "pickup_state": "",
      "pickup_city": "Da Nang",
      "pickup_province": "Da Nang",
      "pickup_postal": "700000",
      "pickup_country": "Viet Nam",
      "pickup_address": "101 Bui Quang La",
      "status": "Pending",
      "created_at": "2021-09-21T04:34:43.083Z",
      "updated_at": "2021-09-21T04:34:43.083Z",
      "first_mile": "yes",
      "freight_forwarder": "no",
      "custom_brokerages": "yes",
      "last_mile": "yes",
      "order_state": "custom_brokerages",
      "order_items": [
        {
          "description": "Fuhlen bluetooth mouse",
          "quantity": 11,
          "product_code": "446",
          "sku": "PO-918242213653",
          "category": "Electronics",
          "price": "55.0",
          "currency": "usd",
          "created_at": "2021-09-21T04:34:43.086Z",
          "updated_at": "2021-09-21T04:34:43.086Z"
        },
        {
          "description": "Logitech MX2 Anywhere Bluetooth mouse",
          "quantity": 1200,
          "product_code": "446",
          "sku": "PO-918242213418",
          "category": "Electronics",
          "price": "600.0",
          "currency": "usd",
          "created_at": "2021-09-21T04:34:43.087Z",
          "updated_at": "2021-09-21T04:34:43.087Z"
        }
      ]
    }
  ]
}

```
