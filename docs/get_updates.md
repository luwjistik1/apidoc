#Get Updates API

Get all the order updates


## Endpoint

```
GET {{base_url}}/api/v1/orders/<tracking number>/updates
```

Replace `<tracking number>` with the tracking number of the order being queried.


## Status List

- FMPickUpRequested, FMPickedUpAtOrigin, FMPickUpFailed, FMHubTransfer, FMReceivedAtMainHub,
- FMDeliveredToFreightForwarder, FMDeliveryFailed, FMShipmentLost, FMShipmentDamaged,
- FFReceived, FFReceivedFailed,
- CCReceivedFailed, CCPendingCustomClearance, CCCustomClearanceOnHold, CCCustomCleared, CCHandoverToLastMile,
- CCShipmentLost, CCShipmentDamaged,
- LMReceivedAtDestinationWarehouse, LMReceivedFailed, LMDelivery, LMHubTransfer, LMDeliveredToCustomer,
- LMFailed, LMReturnedToDestinationWarehouse, LMReturnedToSender, LMShipmentLost, LMShipmentDamaged

## Request Header

```cURL
api-key: <Token>
Content-Type: application/json
```


## Request Body

N/A

## Request Response

```json 
{
  "data": [
    {
      "status": "FMPickUpRequested",
      "partner_code": DHL,
      "created_at": "Tue, 14 Sep 2021 15:23:19.991894000 UTC +00:00",
      "updated_at": "Tue, 14 Sep 2021 15:23:19.991894000 UTC +00:00",
      "reason": ""
    },
    {
      "status": "FMPickedUpAtOrigin",
      "partner_code": DHL,
      "created_at": "Tue, 14 Sep 2021 15:23:23.862537000 UTC +00:00",
      "updated_at": "Tue, 14 Sep 2021 15:23:23.862537000 UTC +00:00",
      "reason": ""
    },
    {
      "status": "FMHubTransfer",
      "partner_code": DHL,
      "created_at": "Tue, 14 Sep 2021 15:23:27.923934000 UTC +00:00",
      "updated_at": "Tue, 14 Sep 2021 15:23:27.923934000 UTC +00:00",
      "reason": ""
    },
    {
      "status": "FMReceivedAtMainHub",
      "partner_code": DHL,
      "created_at": "Tue, 14 Sep 2021 15:23:33.371859000 UTC +00:00",
      "updated_at": "Tue, 14 Sep 2021 15:23:33.371859000 UTC +00:00",
      "reason": ""
    },
    {
      "status": "FMDeliveredToFreightForwarder",
      "partner_code": DHL,
      "created_at": "Tue, 14 Sep 2021 15:23:38.356759000 UTC +00:00",
      "updated_at": "Tue, 14 Sep 2021 15:23:38.356759000 UTC +00:00",
      "reason": ""
    },
    {
      "status": "FFReceived",
      "partner_code": DHL,
      "created_at": "Tue, 14 Sep 2021 15:23:42.688952000 UTC +00:00",
      "updated_at": "Tue, 14 Sep 2021 15:23:42.688952000 UTC +00:00",
      "reason": ""
    },
    {
      "status": "CCPendingCustomClearance",
      "partner_code": njv,
      "created_at": "Tue, 14 Sep 2021 15:24:31.109014000 UTC +00:00",
      "updated_at": "Tue, 14 Sep 2021 15:24:31.109014000 UTC +00:00",
      "reason": ""
    },
    {
      "status": "CCCustomClearanceOnHold",
      "partner_code": njv,
      "created_at": "Tue, 14 Sep 2021 15:24:44.384141000 UTC +00:00",
      "updated_at": "Tue, 14 Sep 2021 15:24:44.384141000 UTC +00:00",
      "reason": "Additional Document Required"
    },
    {
      "status": "CCCustomCleared",
      "partner_code": njv,
      "created_at": "Tue, 14 Sep 2021 15:24:48.116043000 UTC +00:00",
      "updated_at": "Tue, 14 Sep 2021 15:24:48.116043000 UTC +00:00",
      "reason": ""
    },
    {
      "status": "CCHandoverToLastMile",
      "partner_code": njv,
      "created_at": "Tue, 14 Sep 2021 15:24:54.606235000 UTC +00:00",
      "updated_at": "Tue, 14 Sep 2021 15:24:54.606235000 UTC +00:00",
      "reason": ""
    },
    {
      "status": "LMReceivedAtDestinationWarehouse",
      "partner_code": JNE,
      "created_at": "Tue, 14 Sep 2021 15:24:59.562052000 UTC +00:00",
      "updated_at": "Tue, 14 Sep 2021 15:24:59.562052000 UTC +00:00",
      "reason": ""
    },
    {
      "status": "LMDelivery",
      "partner_code": JNE,
      "created_at": "Tue, 14 Sep 2021 15:25:09.612132000 UTC +00:00",
      "updated_at": "Tue, 14 Sep 2021 15:25:09.612132000 UTC +00:00",
      "reason": ""
    },
    {
      "status": "LMHubTransfer",
      "partner_code": JNE,
      "created_at": "Tue, 14 Sep 2021 15:25:13.565335000 UTC +00:00",
      "updated_at": "Tue, 14 Sep 2021 15:25:13.565335000 UTC +00:00",
      "reason": ""
    },
    {
      "status": "LMDeliveredToCustomer",
      "partner_code": JNE,
      "created_at": "Tue, 14 Sep 2021 15:25:22.809649000 UTC +00:00",
      "updated_at": "Tue, 14 Sep 2021 15:25:22.809649000 UTC +00:00",
      "reason": ""
    }
  ]
}
```
